process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";
const mediasoup = require('mediasoup');
const fs = require('fs');
const https = require('https');
const express = require('express');
const socketIO = require('socket.io');
const config = require('./config1_RRJ_Slave');
const Process = require("child_process");
//const log = require("log-to-file");

const io_master = require('socket.io-client');
const MAX_STUDENT_COUNT = 50;

let masterSocket;
let studentSocket = 0;

var Connection = require('tedious').Connection;
var Request = require('tedious').Request;
var TYPES = require('tedious').TYPES;


// Create connection to database
let config_db = {
        server: 'vsieduproduct2020.database.windows.net',
        authentication: {
                type: 'default',
                options: {
                        userName: 'vsi_admin',
                        password: 'productedu@2020'
                }
        },
        options: {
                database: 'vsiedudb2020'
        }
};



var connection = new Connection(config_db);




// Global variables
let worker;
let webServer;
let socketServer;
let expressApp;
let producer;
let consumer;
let producerTransport;
let consumerTransport;
let mediasoupRouter;
let vProducer;
let aProducer;

let audioPlainTransport;
let videoPlainTransport;
let audioPlainConsumer;
let videoPlainConsumer;
let data_ID;
let gFlagVideo = 1;
let gFlagAudio = 1;

let flag = 0;
var consumer_dict = {};
let gConsumerCount = 0;
let gRouterNumber = 0;
var studentCountPerRouter = new Array();

let remotePipeTransport;
let audioPipeProducer;
let videoPipeProducer;


var mediasoupRouters =  new Array();
var workers = new Array();
var worker_status = new Array();
var publish_dict = {};
var vProducers = {};
var aProducers = {};
var pTransports = {};
var cTransports = {};
var mapId = {};


(async () => {
  try {

	for(var i = 0; i < 10; i++){
		studentCountPerRouter[i] = 0;
	}

    await runExpressApp();
    await runWebServer();
    await runSocketServer();
    for (var i=0; i<10; i++)
    {
      worker_status[i] = 0; 
      await runMediasoupWorker(i);
    }
//    await runMediasoupWorker();

    await connectToMasterServer();
    await ConnectToDatabase();

  } catch (err) {
    console.log(err);
  }
})();



async function ConnectToDatabase(){


        connection.on('connect', function(err) {
          if (err) {
                      console.log(err);
                    }
        else {
                console.log('Connected');
        }
        });

        connection.connect();

}


function addServerRecord(ipAddress, connectionCount) {
    //let ipAddress = '52.147.200.215';
    //let connectionCount = 0;

    request = new Request('INSERT INTO vsiedu.SlaveServers (PublicIpAddress, ConnectionCount) VALUES (@ip, @count);', (err, rowCount) => {
        if (err) {
            console.log(err.message);
        } else {
            console.log(rowCount + ' row(s) inserted');
        }
    });

        request.on('requestCompleted', function() {
                getServerRecords();
        });

    request.addParameter('ip', TYPES.NVarChar, ipAddress);
    request.addParameter('count', TYPES.Int, connectionCount);

    connection.execSql(request);
	console.log("\n");
    console.log("addServerRecord: Public Ip: ", ipAddress);
    console.log("addServerRecord: Connection Count: ", connectionCount);
}



function updateServerRecord(ipAddress, newConnectionCount) {
  //  let ipAddress = '52.147.200.215';
  //  let newConnectionCount = 10;

    request = new Request('UPDATE vsiedu.SlaveServers SET ConnectionCount = @count WHERE PublicIpAddress = @ip;', (err, rowCount) => {
        if (err) {
            console.log(err.message);
        } else {
            console.log(" " + rowCount + ' row(s) updated');
        }
    });


        request.on('requestCompleted', function() {
                getServerRecords();
        });

    request.addParameter('count', TYPES.Int, newConnectionCount);
    request.addParameter('ip', TYPES.NVarChar, ipAddress);

    connection.execSql(request);

	console.log("\n");
    console.log("updateServerRecord: Public Ip: ", ipAddress);
    console.log("updateServerRecord: Connection Count: ", newConnectionCount);
}

function deleteServerRecord(ipAddress) {


    request = new Request('DELETE FROM vsiedu.SlaveServers WHERE PublicIpAddress = @ip;', (err, rowCount) => {
        if (err) {
            console.log(err.message);
        } else {
            console.log(rowCount + ' row(s) deleted');
        }
    });


        request.on('requestCompleted', function() {
                getServerRecords();
        })

    request.addParameter('ip', TYPES.NVarChar, ipAddress);
    console.log("deleteServerRecord: Ip: ", ipAddress);

    connection.execSql(request);
}



function getServerRecords() {
    request = new Request('SELECT * FROM vsiedu.SlaveServers;', (err, rowCount) => {
        if (err) {
            console.log(err.message);
        } else {
            console.log(rowCount + ' row(s) returned\n');
        }
    });

    var result = "";
    request.on('row', (columns) => {
        columns.forEach(element => {
            if (element.value === null) {
                console.log('NULL');
            } else {
                result += element.value + " ";
            }
        });

        console.log(result);
        result = "";
    });

    connection.execSql(request);
}






async function runExpressApp() {
  expressApp = express();
  expressApp.use(express.json());
  expressApp.use(express.static(__dirname));

  expressApp.use((error, req, res, next) => {
    if (error) {
      console.warn('Express app error,', error.message);

      error.status = error.status || (error.name === 'TypeError' ? 400 : 500);

      res.statusMessage = error.message;
      res.status(error.status).send(String(error));
    } else {
      next();
    }
  });
}

async function runWebServer() {
  const { sslKey, sslCrt } = config;
  if (!fs.existsSync(sslKey) || !fs.existsSync(sslCrt)) {
    console.log('SSL files are not found. check your config.js file');
    process.exit(0);
  }
  const tls = {
    cert: fs.readFileSync(sslCrt),
    key: fs.readFileSync(sslKey),
  };
  webServer = https.createServer(tls, expressApp);
 // webServer = https.createServer( expressApp);
  webServer.on('error', (err) => {
    console.log('starting web server failed:', err.message);
  });

  await new Promise((resolve) => {
    const { listenIp, listenPort } = config;
    webServer.listen(listenPort, listenIp, () => {
      const listenIps = config.mediasoup.webRtcTransport.listenIps[0];
      const ip = listenIps.announcedIp || listenIps.ip;
      console.log('server is running');
      console.log(`open https://${ip}:${listenPort} in your web browser`);
      resolve();
    });
  });
}

async function runSocketServer() {
  socketServer = socketIO(webServer, 
	   {
		     cors: {
			         origins: ['http://localhost:4200']
			       },
/*		   
	  {
    cors: {
     origin: "*",
     methods: ["GET", "POST"],   
     credentials: true
    }
    ,
    */
    serveClient: false,
    path: '/server',
    log: false,
  });

  socketServer.on('connection', (socket) => {
    
    console.log("\n");
    console.log('client connected --');  
    socket.emit("ack");
    socket.on('subscribe', async(data, callback) => {
      console.log('subscribed       ', data.id);
      count = 1;
      for (i in cTransports)
        count++;
      socket.broadcast.emit ("user", {name:data.name, total:count});
      var did;
      if (data.id in mapId)
         did = mapId[data.id] 
         if (did in publish_dict)
             socket.emit ("newProducer");
      callback (data);
    });
      
    socket.on('disconnect', () => {
      console.log('\ndisconnect called');
      if (socket.id in publish_dict)
      {
         const i =  publish_dict[socket.id];
         worker_status[i] = 0;
         delete publish_dict[socket.id];
         console.log('stopStream Called Publisher');
         socket.broadcast.emit ("stopStream");
      }
      else if (socket.id in cTransports)
      { 
	
	let j = consumer_dict[socket.id];
	studentCountPerRouter[j] -= 1;
	gConsumerCount--;
	
	if(gConsumerCount < 0)
	      gConsumerCount = 0;

	try{
		updateServerRecord(config.mediasoup.webRtcTransport.listenIps[0].announcedIp, gConsumerCount);

	}
	catch(err){
		console.log(err);
	}


	console.log("Consumer Connected to Router " + j + " Disconnected"); 
	console.log("Router: " + j + " Have : " + studentCountPerRouter[j])

        console.log('closing the Subsrciber client transport ');      
        cTransports[socket.id].close();
        delete cTransports[socket.id];
        count = 0;
        for (i in cTransports)
            count++;
        socket.broadcast.emit ("user_remove", {name:socket.id, total:count});
      }
      else{

	let j = consumer_dict[socket.id];
	studentCountPerRouter[j] -= 1;
	gConsumerCount--;

	if(gConsumerCount < 0)
	      gConsumerCount = 0;

	try{
		
		//updateServerRecord(config.mediasoup.webRtcTransport.listenIps[0].announcedIp, gConsumerCount - 1);
		updateServerRecord(config.mediasoup.webRtcTransport.listenIps[0].announcedIp, gConsumerCount);
	}
	catch(err){
		console.log(err);
	}
      
      }
	
	/*let j = consumer_dict[socket.id];
	studentCountPerRouter[j] -= 1;
	gConsumerCount--;


	console.log("Consumer Connected to Router " + j + " Disconnected"); 
	console.log("Router: " + j + " Have : " + studentCountPerRouter[j])*/
 
	console.log('client disconnected');
    });

    socket.on('connect_error', (err) => {
      console.log('client connection error', err);
        });

    socket.on('close_client', () => {
      console.log('@@@@@@ socket close');
	    socket.destroy();
    });
    socket.on('add_class', async(data, callback) => {
            console.log('Adding class  ', socket.id, data.id);
            console.log(socket.id);
            mapId[data.id] = socket.id;

            data_ID = data.id;

            for (var i=0; i<10; i++)
            {
                if (worker_status[i] == 0)
                {
                    publish_dict[socket.id] = i;
                    worker_status[i] = 1;
                    break;
                }
            }                
            callback ({id:socket.id});

    //         	if(flag == 1){

	   //          	client_id = socket.id;

	   //          	if(gConsumerCount % 2 == 0){
		  // 		gRouterNumber++;
		  // 		console.log("Router Changes to -> " + gRouterNumber + "\n");
		  // 	}

		  // 	consumer_dict[client_id] = gRouterNumber;
		  // 	gConsumerCount++;
		  	
	  	// }


    
     //       //  const atransportOptions = {listenIp: { ip: '0.0.0.0', announcedIp: undefined }};
     //       //  const aplainTransportOptions = 
     //       //  {
     //       //      ...config.plainTransport,
     //       //      rtcpMux: false,
     //       //      comedia: true
     //       //  };
     //       //  const atransport = await mediasoupRouters[i].createPlainTransport(aplainTransportOptions);

     //       //  //console.log(atransport);
     //       //  console.log(atransport.id);
     //       //  console.log(atransport.tuple.localIp);
     //       //  console.log(atransport.tuple.localPort);
        
     //       //  const artpParameters =  { 
     //       //  codecs: 
     //       //  [
     //       //      { 
     //       //          mimeType : "audio/opus",  
     //       //          payloadType :101,                     
     //       //          clockRate :48000,                     
     //       //          channels :2,                         
     //       //          parameters :{  'sprop-stereo' :1 } }
     //       // ], 
     //       //  encodings : [{ ssrc : 1111 }] 
     //       //  };

     //       //  var kind = 'audio';

     //       //  try
     //       //  {
     //       //      const producer1 = await atransport.produce(
     //       //      {
     //       //          kind: 'audio', 
     //       //          rtpParameters: artpParameters
     //       //      });
     //       //      aProducers[socket.id] = producer1;
     //       //  }
     //       //  catch (error)
     //       //  {
     //       //      console.log(error);
     //       //  }
            
     //       //  const vtransport = await mediasoupRouters[i].createPlainTransport(aplainTransportOptions);

     //       //  //console.log(vtransport);
     //       //  console.log(vtransport.id);
     //       //  console.log(vtransport.tuple.localIp);
     //       //  console.log(vtransport.tuple.localPort);
        
     //       //  const vrtpParameters =  { 
     //       //  codecs: 
     //       //  [
     //       //      { 
     //       //          mimeType : "video/vp8",  
     //       //          clockRate :90000,                     
     //       //          payloadType :100                     
     //       //      }
     //       // ],
     //       //  encodings : [{ ssrc : 2222 }] 
     //       //  };

     //       //  try
     //       //  {
     //       //      const producer1 = await vtransport.produce(
     //       //      {
     //       //          kind: 'video', 
     //       //          rtpParameters: vrtpParameters
     //       //      });
     //       //      vProducers[socket.id] = producer1;

     //       //     // console.log(producer1)
     //       //  }
     //       //  catch (error)
     //       //  {
     //       //      console.log(error);
     //       //  }
            
     //       //  startAudioProducerGstreamer(atransport.tuple.localPort);
     //       //  startVideoProducerGstreamer(vtransport.tuple.localPort);
            

     //        //Connect Transport 

     //        //Start Produce




     //        // Consumer Transport
     //        const atransportOptions_consumer = {listenIp: { ip: '127.0.0.1', announcedIp: undefined }};
     //        const aplainTransportOptions_consumer = 
     //        {
     //            ...config.plainTransport,
     //            rtcpMux: false,
     //            comedia: false
     //        };
            
     //        audioPlainTransport = await mediasoupRouters[i].createPlainTransport(aplainTransportOptions_consumer);

     //        //console.log(atransport);
     //        console.log(audioPlainTransport.id);
     //        console.log(audioPlainTransport.tuple.localIp);
     //        console.log(audioPlainTransport.tuple.localPort);
        
     //       //  const artpParameters_consumer =  { 
     //       //  codecs: 
     //       //  [
     //       //      { 
     //       //          mimeType : "audio/opus",  
     //       //          payloadType :101,                     
     //       //          clockRate :48000,                     
     //       //          channels :2,                         
     //       //          parameters :{  'sprop-stereo' :1 } }
     //       // ], 
     //       //  encodings : [{ ssrc : 1111 }] 
     //       //  };

     //       //  kind = 'audio';

     //    //     try
     //    //     {
     //    //         const consumer1 = await audioPlainTransport.consume(
     //    //         {
		   //    // producerId: aProducers[socket.id],
		   //    // artpParameters,
     //    //         });
     //    //         audioPlainConsumer = consumer1;
     //    //         console.log("consume audio\n");

     //    //     }
     //    //     catch (error)
     //    //     {
     //    //         console.log(error);
     //    //     }


     //        videoPlainTransport = await mediasoupRouters[i].createPlainTransport(aplainTransportOptions_consumer);

     //        //console.log(vtransport);
     //        console.log(videoPlainTransport.id);
     //        console.log(videoPlainTransport.tuple.localIp);
     //        console.log(videoPlainTransport.tuple.localPort);
        
     //       //  const vrtpParameters =  { 
     //       //  codecs: 
     //       //  [
     //       //      { 
     //       //          mimeType : "video/vp8",  
     //       //          clockRate :90000,                     
     //       //          payloadType :100                     
     //       //      }
     //       // ],
     //       //  encodings : [{ ssrc : 2222 }] 
     //       //  };

     //       await audioPlainTransport.connect({ip: '127.0.0.1',
					//     port: 5000,
					//     rtcpPort: 5002});

     //      console.log("connecting Audio consumer transport done");

     //      console.log(
	    //   "mediasoup AUDIO RTP SEND transport connected: %s:%d <--> %s:%d (%s)",
	    //   audioPlainTransport.tuple.localIp,
	    //   audioPlainTransport.tuple.localPort,
	    //   audioPlainTransport.tuple.remoteIp,
	    //   audioPlainTransport.tuple.remotePort,
	    //   audioPlainTransport.tuple.protocol
	    // );


     //      await videoPlainTransport.connect({ip: '127.0.0.1',
					//     port: 5004,
					//     rtcpPort: 5006});

     //       console.log(
	    //   "mediasoup VIDEO RTP SEND transport connected: %s:%d <--> %s:%d (%s)",
	    //   videoPlainTransport.tuple.localIp,
	    //   videoPlainTransport.tuple.localPort,
	    //   videoPlainTransport.tuple.remoteIp,
	    //   videoPlainTransport.tuple.remotePort,
	    //   videoPlainTransport.tuple.protocol
	    // );

     //      console.log("connecting Video consumer transport done");



          //await socket.emit('add_class_slave', {id: data.id});

          // console.log("Add_Class_Slave emited");

          


    });


	// socket.on('LoadDevice_Slave', async(data, callback) => {

	// 	socket.emit('add_class_slave', {id : data.id});

	// });


    socket.on('register_client', async(data, callback) => {  	  
      console.log('Adding subscriber  client  ', socket.id);
      console.log(socket.id);

        cid = socket.id;

      	//if(gConsumerCount % MAX_STUDENT_COUNT  == 0){
      	//	gRouterNumber++;
      		//console.log("Router Changes to -> " + gRouterNumber + "\n");
      	//}

	//console.log("Student: " + gConsumerCount);
	//console.log("Roter:" + gRouterNumber);

	for(var i = 0; i < 10; i++){
		if(studentCountPerRouter[i] < MAX_STUDENT_COUNT){
			studentCountPerRouter[i] += 1;
			consumer_dict[cid] = i;

			//gConsumerCount += studentPerRouter[i];
	
			gConsumerCount++

			try{
				//updateServerRecord("104.211.4.33", gConsumerCount);
				updateServerRecord(config.mediasoup.webRtcTransport.listenIps[0].announcedIp, gConsumerCount);
			}
			catch(err){
				console.log(err);
			}

			console.log("Student: " + gConsumerCount);
			console.log("Roter:" + i);
			//gConsumerCount++;




			break;
		}
		//else if(studentPerRouter[i] == MAX_STUDENT_COUNT){
		//	gConsumerCount += studentPerRouter[i];
		//}
	}
	

      	
	//consumer_dict[cid] = gRouterNumber;
      	//gConsumerCount++;

	//     console.log("gRouterNumber -> " + gRouterNumber + " gConsumerCount -> " + gConsumerCount + " consumer_dict -> " + consumer_dict[cid]);
	 
//	console.log("Student: " +( gConsumerCount-1);
	//console.log("Roter:" + gRouterNumber;
	
	//const ip1 = config.mediasoup.webRtcTransport.listenIps[0];
	  //  console.log("ip1 : ", ip1.ip);
	    //console.log(`ip1 : //${ip1}`);

	
	 const privateIP =  config.mediasoup.webRtcTransport.listenIps[0].ip;
	const publicIP = config.mediasoup.webRtcTransport.listenIps[0].announcedIp;
	 console.log("Private IP: " , privateIP);
	console.log("Public IP: " , publicIP);

      callback ({id:socket.id, public_ip: publicIP, private_ip: privateIP});
    });
      
    socket.on('resume_streams', async(data, callback) => {
      socket.broadcast.emit('refresh_streams');
      callback ({id:data.id});
    });

    socket.on('resume_audio', async(data, callback) => {
      socket.broadcast.emit('refresh_audio');
      callback ({id:data.id});
    });

    socket.on('resume_video', async(data, callback) => {
      socket.broadcast.emit('refresh_video');
      callback ({id:data.id});
    });

    socket.on('notify_stopvideo', async(data, callback) => {
      socket.broadcast.emit('removeVideo');
      callback ({id:data.id});
    });

    socket.on('getRouterRtpCapabilities', (data, callback) => {
      var did;	
	    console.log("dataID " + data.id);
      if (data.id in mapId)
      {
          did = mapId[data.id];
      }
      else
      {
	  //gConsumerCount--;
	  //if(gConsumerCount % MAX_STUDENT_COUNT == 0)
	  //    gRouterNumber--;

	 
	  //let i = consumer_dict[data.client_id];
	  /*gConsumerCount--;
	  studentCountPerRouter[i] -= 1;


	if(gConsumerCount < 0)
	      gConsumerCount = 0;

	try{
		//updateServerRecord("104.211.4.33", gConsumerCount);
		//updateServerRecord(config.mediasoup.webRtcTransport.listenIps[0].announcedIp, gConsumerCount - 1);
		updateServerRecord(config.mediasoup.webRtcTransport.listenIps[0].announcedIp, gConsumerCount);
	}
	catch(err){
		console.log(err);
	}*/


          callback({ error: "Meeting not started yet please try after some time 1"+data.id });  
          return;
      }
        
      if (did in publish_dict)
      {
      	  // if(flag == 0){
         //  	const id = publish_dict[did];
         //  	flag++;
         //  	console.log("flag = 0");
         //  	console.log("called   ", data.id, id);
         //  	callback(mediasoupRouters[id].rtpCapabilities);
         //  }
         //  else if(flag == 1){

         //  	// if(gConsumerCount % 2 == 0){
         //  	// 	gRouterNumber++;
         //  	// 	console.log("Router Changes to -> " + gRouterNumber + "\n");
         //  	// }

         //  	// consumer_dict[data.client_id] = gRouterNumber;
         //  	// gConsumerCount++;


         //  	const id = consumer_dict[data.client_id];
         //  	console.log("flag = 1");
         //  	console.log("called   ", data.id, id, data.client_id);
         //  	callback(mediasoupRouters[id].rtpCapabilities);
         //  }	


          if(data.type == 'client'){
		studentSocket = socket;
          	const id = consumer_dict[data.client_id];
          	console.log("called  Client ", data.id, id, data.client_id);
          	callback(mediasoupRouters[id].rtpCapabilities);
          }
          else{
          	const id = publish_dict[did];
          	//flag++;
          	console.log("flag = 0");
          	console.log("called  Producer ", data.id, id);
          	callback(mediasoupRouters[id].rtpCapabilities);
          }

          console.log("Data Type: " + data.type);


          
      }
      else
      {      

	//gConsumerCount--;
	//if(gConsumerCount % MAX_STUDENT_COUNT == 0)
	  //    gRouterNumber--;
	
	/*let i = consumer_dict[data.client_id] ;
	gConsumerCount--;
	studentCountPerRouter[i] -= 1;

	if(gConsumerCount < 0)
	      gConsumerCount = 0;

	try{
		//updateServerRecord("104.211.4.33", gConsumerCount);
		//updateServerRecord(config.mediasoup.webRtcTransport.listenIps[0].announcedIp, gConsumerCount - 1);
		updateServerRecord(config.mediasoup.webRtcTransport.listenIps[0].announcedIp, gConsumerCount);
	}
	catch(err){
		console.log(err);
	}*/
	      
        callback({ error: "Meeting not started yet please try after some time 2" });
      }
    });

    socket.on('createProducerTransport', async (data, callback) => {
      try {
          console.log("create Producer Transport " + data.id);
          const did = mapId[data.id];
          if (did in publish_dict)
          {
              const id = publish_dict[did];
              const { transport, params } = await createWebRtcTransport(id);
              producerTransport = transport;
              pTransports[did] = transport;
              callback(params);
          }
          else
          {
              console.log("No producer available");
          }
      } catch (err) {
        console.log(err);
        callback({ error: err.message });
      }
    });

    socket.on('createConsumerTransport', async (data, callback) => {
      try {
          const did = mapId[data.id];
          
          if (did in publish_dict)
          {
            //const id = publish_dict[did];
            const id = consumer_dict[data.client_id];
          	
            console.log("create Consumer Transport ", did, data.id, id, data.client_id);
            const { transport, params } = await createWebRtcTransport(id);
            consumerTransport = transport;
            cTransports[data.client_id] = transport;
            callback(params);
          }
          else
          {
            console.log("No published id present");
            callback ({error: "No publisher id"})

          }
       } catch (err) {
        console.log(err);
        callback({ error: err.message });
       }
    });

    socket.on('connectProducerTransport', async (data, callback) => {
          const did = mapId[data.id];
          pTransport = pTransports[did]; 
          console.log("connecting producer transport");
          await producerTransport.connect({ dtlsParameters: data.dtlsParameters });
          callback();
          console.log("connecting producer transport done" + data.dtlsParameters);


    });

    socket.on('connectConsumerTransport', async (data, callback) => {
      if (data.client_id in cTransports)
      {
          //console.log("connecting consumer tranaport");
          await cTransports[data.client_id].connect({ dtlsParameters: data.dtlsParameters });
          callback();
          console.log("connecting consumer transport");
      }
    });



   socket.on('SendAudioRTP', async(data, callback) => {

   	kind = data.kind;
   	rtpCapabilities = data.rtpCapabilities;
   	did = mapId[data.id];


   	try{
   		console.log("in SendAudioRTP " + kind, data.id, did);

    		if (did in publish_dict)
    		{

    		  console.log("a if2");	
    		  if (did in aProducers)
    		  {

    		     console.log("a if3");		
    		     aProducer = aProducers[did];
    		     const id = publish_dict[did];

    		     	if (!mediasoupRouters[id].canConsume({
    			      producerId: aProducer.id,
    			      rtpCapabilities,
    			})) {
    			    console.log('can not consume');
    			    return;
    			 }



    		    audioPlainConsumer = await audioPlainTransport.consume({
    		      producerId: aProducer.id,
    		     rtpCapabilities
    		    });	


    		    //await socket.emit('CreateAudioProducer', {payloadType:audioPlainConsumer.rtpParameters.codecs[0].payloadType, ssrc:audioPlainConsumer.rtpParameters.encodings[0].ssrc});

    		    console.log("Master Audio RTP consume");
    		  }
    		  else
    		  {
    		     console.log("else 1");	
    		     //callback({error: "no audio producer"});
    		     return;	  
    		  }
    		}
    		else
    		{
    			console.log("else 2");
    		 	//callback({error: "no audio producer"});
    		 	return;	  
    		}
	   }
  	catch (error) {
  	    console.log('audio consume failed', error);
  	    return;
  	 }

   });


   socket.on('SendVideoRTP', async(data, callback) => {

   	kind = data.kind;
   	rtpCapabilities = data.rtpCapabilities;
   	did = mapId[data.id];


   	try{
   		console.log("in SendVideoRTP " + kind, data.id, did);

		if (did in publish_dict)
		{
			console.log("v if2");
		  if (did in vProducers)
		  {
		  	console.log("v if3");
		     vProducer = vProducers[did];
		     const id = publish_dict[did];

		     	if (!mediasoupRouters[id].canConsume({
			      producerId: vProducer.id,
			      rtpCapabilities,
			})) {
			    console.log('can not consume');
			    return;
			 }


		    videoPlainConsumer = await videoPlainTransport.consume({
		      producerId: vProducer.id,
		     rtpCapabilities
		    });


		    //await socket.emit('CreateVideoProducer', {payloadType:videoPlainConsumer.rtpParameters.codecs[0].payloadType, ssrc:videoPlainConsumer.rtpParameters.encodings[0].ssrc});

		    console.log("Master Video RTP consume");	
		  }
		  else
		  {
		  	console.log("v else 1");
		     //callback({error: "no video producer"});
		     return;	  
		  }
		}
		else
		{
			console.log("v else 2");
		 	//callback({error: "no video producer"});
		 	return;	  
		}
   	}
   	catch (error) {
	    console.log('video consume failed', error);
	    return;
	 }

   });



    socket.on('SendRTPConsume', async(data, callback) => {

    	kind = data.kind;
    	rtpCapabilities = data.rtpCapabilities;
    	//did = mapId[data.id];
      	try{

      		const did = mapId[data.id];



      		console.log("in SendRTPConsume " + kind, data.id, did);

      		if(kind == 'audio'){

      			console.log("a if");

      			if (did in publish_dict)
      			{

      			  console.log("a if2");	
      			  if (did in aProducers)
      			  {

      			     console.log("a if3");		
      			     aProducer = aProducers[did];
      			     const id = publish_dict[did];

      			     	if (!mediasoupRouters[id].canConsume({
      				      producerId: aProducer.id,
      				      rtpCapabilities,
      				})) {
      				    console.log('can not consume');
      				    return;
      				 }



      			    audioPlainConsumer = await audioPlainTransport.consume({
      			      producerId: aProducer.id,
      			     rtpCapabilities
      			    });	


      			    //socket.emit('CreateAudioProducer', {payloadType:audioPlainConsumer.rtpParameters.codecs[0].payloadType, ssrc:audioPlainConsumer.rtpParameters.encodings[0].ssrc});

      			    console.log("Master Audio RTP consume");
      			  }
      			  else
      			  {
      			     console.log("else 1");	
      			     //callback({error: "no audio producer"});
      			     return;	  
      			  }
      			}
      			else
      			{
      				console.log("else 2");
      			 	//callback({error: "no audio producer"});
      			 	return;	  
      			}
      		}


      		if(kind == 'video'){
      			console.log("v if");

      			if (did in publish_dict)
      			{
      				console.log("v if2");
      			  if (did in vProducers)
      			  {
      			  	console.log("v if3");
      			     vProducer = vProducers[did];
      			     const id = publish_dict[did];

      			     	if (!mediasoupRouters[id].canConsume({
      				      producerId: vProducer.id,
      				      rtpCapabilities,
      				})) {
      				    console.log('can not consume');
      				    return;
      				 }


      			    videoPlainConsumer = await videoPlainTransport.consume({
      			      producerId: vProducer.id,
      			     rtpCapabilities
      			    });


      			    //await socket.emit('CreateVideoProducer', {payloadType:videoPlainConsumer.rtpParameters.codecs[0].payloadType, ssrc:videoPlainConsumer.rtpParameters.encodings[0].ssrc});

      			    console.log("Master Video RTP consume");	
      			  }
      			  else
      			  {
      			  	console.log("v else 1");
      			     //callback({error: "no video producer"});
      			     return;	  
      			  }
      			}
      			else
      			{
      				console.log("v else 2");
      			 	//callback({error: "no video producer"});
      			 	return;	  
      			}
      		}
      	}catch (error) {
      	    console.log('consume failed', error);
      	    return;
      	 }

    });



    socket.on('produce', async (data, callback) => {
      const {id, kind, rtpParameters} = data;
      const did = mapId[id];
      producer = await producerTransport.produce({ kind, rtpParameters });
      if (kind == 'video')
      {
        vProducer = producer;
        vProducers[did] = vProducer;

       //  for(var i = 1; i <10; i++){
      	// 	await mediasoupRouters[0].pipeToRouter({ producerId: vProducer.id, router: mediasoupRouters[i]});
      	// 	console.log("Video : pipeToRouter Router 0 -> Router " + i);
      	// }

      }
      else
      {
        aProducer = producer;
        aProducers[did] = aProducer;

        // for(var i = 1; i <10; i++){
      		// await mediasoupRouters[0].pipeToRouter({ producerId: aProducer.id, router: mediasoupRouters[i]});
      		// console.log("Audio : pipeToRouter Router 0 -> Router " + i);
     	  // }
     }

      callback({ id: producer.id });

      console.log("Producer called with ", producer.kind);
    });

    socket.on('consume', async (data, callback) => {
      callback(await createConsumer(0, 0, producer, data.rtpCapabilities, 'video'));
    });

    socket.on('consume_video', async (data, callback) => {
      const did = mapId[data.id];
      console.log("V Publisher ID   ", data.id, did, data.client_id);
      if (did in publish_dict)
      {	      
	  if (did in vProducers)
	  {
        vProducer = vProducers[did];
	     //const id = publish_dict[did];
	     
	     const id = consumer_dict[data.client_id];

	     console.log("V " + id, did, data.id);
	     callback(await createConsumer(id, data.client_id,  vProducer, data.rtpCapabilities, 'video'));
	  }
	  else
	  {	  
             callback({error: "no video producer"});
	     return;	  
	  }
      }
      else
      {
         callback({error: "no video producer"});
	 return;	  
      }
    });

    socket.on('consume_audio', async (data, callback) => {
      console.log("A Publisher ID   ", data.id);
      const did = mapId[data.id];
      if (did in publish_dict)
      {
	  if (did in aProducers)
	  {
             aProducer = aProducers[did];
             
             //const id = publish_dict[did];
             const id = consumer_dict[data.client_id];


             console.log("A " + id, did, data.id);
             callback(await createConsumer(id, data.client_id, aProducer, data.rtpCapabilities, 'audio'));
	  }
	  else
	  {
             callback({error: "no audio producer"});
	     return;	  
	  }
      }
      else
      {
         callback({error: "no audio producer"});
	 return;	  
      }
    });
    socket.on('resume', async (data, callback) => {
      await consumer.resume();
      callback();
    });
  });
}





async function connectToMasterServer(){

	console.log("ConnectToMasterServer: Enter");

  const opts = {
      path: '/server',
      transports: ['websocket'],
      rejectUnauthorized: false
    };

  masterSocket = io_master("https://20.81.32.179:5001", opts);
  //masterSocket.request = promise(masterSocket);
  //masterSocket.emit('my message', 'Hello there Slave Server.');

  masterSocket.on('connection', () => {
    console.log("Slave Connected to Master");
  });

  masterSocket.on('ack', async() => {
      console.log("acknowledges");

      remotePipeTransport = await mediasoupRouters[0].createPipeTransport({
          listenIp: "0.0.0.0",
          enableSctp: true,
          numSctpStreams: { OS: 1024, MIS: 1024 }, 
          enableRtx: false, 
          enableSrtp: false 
        });       

      console.log("remotePipeTransport: ");
      console.log("remotePipeTransport: ");
      console.log(remotePipeTransport.id);
      console.log(remotePipeTransport.tuple.localIp);
      console.log(remotePipeTransport.tuple.localIp);
      console.log(remotePipeTransport.tuple.localPort);
      console.log(remotePipeTransport.tuple.srtpParameters);


      masterSocket.emit('ConnectToPipeTransport', {localIp: config.mediasoup.webRtcTransport.listenIps[0].announcedIp, localPort: remotePipeTransport.tuple.localPort, srtpParameters: remotePipeTransport.tuple.srtpParameters});
	//:const publicIP = config.mediasoup.webRtcTransport.listenIps[0].announcedIp;
  });

  masterSocket.on('connect', async () => {
    console.log("Connecting Device");
  });

  masterSocket.on('disconnect', () =>{
    console.log("Slave Disconneted from Master");


	/*try{
		deleteServerRecord(config.mediasoup.webRtcTransport.listenIps[0].announcedIp);

	}
	catch(err){
		console.log(err);
	}*/
  });

  masterSocket.on('connect_error', (error) =>{
    console.log("Slave connect_error : " + error.message);
  });


  masterSocket.on('error', (error) => {
    console.log("In Error", error.message);
  })


  masterSocket.on('CreateAudioProducer', async(data) => {
      sscr1 = data.ssrc;
      payloadType1 = data.payloadType;

      console.log("CreateAudioProducer: Enter " + data.ssrc, payloadType1);

       const artpParameters =  { 
            codecs: 
            [
                { 
                    mimeType : "audio/opus",  
                    //payloadType :101,                     
                    payloadType :payloadType1,                     
                    //payloadType :100,                     
                    clockRate :48000,                     
                    channels :2,                         
                    parameters :{  'sprop-stereo' :1 } }
           ], 
            encodings : [{ ssrc : sscr1 }] 
            };

            var kind = 'audio';

            console.log("add_class: ap try");

            try
            {
                const producer1 = await aPlainTransport.produce(
                {
                    kind: 'audio', 
                    rtpParameters: artpParameters
                });
                aProducers[gSlaveSocket.id] = producer1;
                aPlainProducer = producer1;

                console.log("aProducers done");
            }
            catch (error)
            {
                console.log(error);
            }


             //startAudioProducerGstreamer(aPlainTransport.tuple.localPort)



      console.log("CreateAudioProducer: Leave");
  });



  masterSocket.on('CreateVideoProducer', async(data) => {
      sscr1 = data.ssrc;
      payloadType1 = data.payloadType;

      console.log("CreateVideoProducer: Enter " + data.ssrc, data.payloadType);


        const vrtpParameters =  { 
            codecs: 
            [
                { 
                    mimeType : "video/H264",  
                    clockRate :90000,                     
                    //payloadType :100,
                    payloadType :payloadType1,
                     parameters: {
                            "level-asymmetry-allowed": 1,
                            "packetization-mode": 1,
                            "profile-level-id": "42e01f",
                      }                     
                }
           ],
            encodings : [{ ssrc : sscr1 }] 
            };

            console.log("add_class: vp try");

            try
            {
                const producer1 = await vPlainTransport.produce(
                {
                    kind: 'video', 
                    rtpParameters: vrtpParameters
                });
                vProducers[gSlaveSocket.id] = producer1;
                vPlainProducer = producer1;

               console.log("vProducers done");
            }
            catch (error)
            {
                console.log(error);
            }


             //startVideoProducerGstreamer(vPlainTransport.tuple.localPort, payloadType1, sscr1)

      console.log("CreateVideoProducer: Leave");

  });


  masterSocket.on('ConnectToPipeTransportSlave', async(data) => {

    console.log("ConnectToPipeTransport Slave: ", data.localIp);
    console.log("ConnectToPipeTransport Slave: ", data.localPort);
    console.log("ConnectToPipeTransport Slave: ", data.srtpParameters);

    remotePipeTransport.connect({
              ip: data.localIp,
              port: data.localPort,
              srtpParameters: data.srtpParameters
        }); 

    console.log("localPipeTransport: Connects to Remote");

  });


  masterSocket.on('SendConsumer', async(data) => {
    
      console.log("SendConsumer: " + data.kind);

      pipeProducer = await remotePipeTransport.produce({
	      id: data.producerID,
          kind: data.kind,
          rtpParameters: data.rtpParameters,
          paused: data.producerPaused,
          appData: data.appData
      });   

      if(data.kind == 'video'){
        videoPipeProducer = pipeProducer;
        console.log("SendConsumer: " + data.kind + " producer done");
        
        for(var i = 1; i <10; i++){
          await mediasoupRouters[0].pipeToRouter({ producerId: videoPipeProducer.id, router: mediasoupRouters[i]});
          console.log(data.kind + " : pipeToRouter Router 0 -> Router " + i);
        }
      }
      else if(data.kind == 'audio'){
        audioPipeProducer = pipeProducer;
        console.log("SendConsumer: " + data.kind + " producer done");

        for(var i = 1; i <10; i++){
          await mediasoupRouters[0].pipeToRouter({ producerId: audioPipeProducer.id, router: mediasoupRouters[i]});
          console.log(data.kind + " : pipeToRouter Router 0 -> Router " + i);
        }

      } 


      

       

  });

   masterSocket.on('publisher_disconnect', (data) => {
   	
	   const i = publish_dict[masterSocket.id];
	   worker_status[i] = 0;
	   delete publish_dict[masterSocket.id];
	   console.log("\n");
	   console.log("Publisher_disconnect called");
	
	   if(studentSocket != 0){
	   	studentSocket.broadcast.emit("stopStream", {id: data.id});
	   	studentSocket.emit("stopStream", {id: data.id});
   	   }
   });


   masterSocket.on("refresh_stream_slave", async(data) => {
   	
	   if(studentSocket != 0){
	   	studentSocket.broadcast.emit("refresh_streams", {id: data.id});
	   	studentSocket.emit("refresh_streams", {id: data.id});
	   	console.log("refresh_stream_slave called");
	   }

   });


   masterSocket.on('add_class_slave', async(data, callback) => {
      
	console.log("\n");
      console.log('Adding class slave ', masterSocket.id, data.id);
      console.log(masterSocket.id);
      mapId[data.id] = masterSocket.id;

      for (var i=0; i<10; i++)
      {
          if (worker_status[i] == 0)
          {
              publish_dict[masterSocket.id] = i;
              worker_status[i] = 1;
              break;
          }
      }   


      vProducers[masterSocket.id] = videoPipeProducer;
      aProducers[masterSocket.id] = audioPipeProducer;             

     //  const atransportOptions = {listenIp: { ip: '127.0.0.1', announcedIp: undefined }};
     //  const aplainTransportOptions = 
     //  {
     //      ...config.plainTransport,
     //      rtcpMux: false,
     //      comedia: true
     //  };
     //  const atransport = await mediasoupRouters[i].createPlainTransport(aplainTransportOptions);

     //  //console.log(atransport);
     //  console.log(atransport.id);
     //  console.log(atransport.tuple.localIp);
     //  console.log(atransport.tuple.localPort);

     //  const artpParameters =  { 
     //  codecs: 
     //  [
     //      { 
     //          mimeType : "audio/opus",  
     //          payloadType :101,                     
     //          clockRate :48000,                     
     //          channels :2,                         
     //          parameters :{  'sprop-stereo' :1 } }
     // ], 
     //  encodings : [{ ssrc : 1111 }] 
     //  };

     //  var kind = 'audio';

     //  try
     //  {
     //      const producer1 = await atransport.produce(
     //      {
     //          kind: 'audio', 
     //          rtpParameters: artpParameters
     //      });
     //      aProducers[masterSocket.id] = producer1;
     //      aPlainProducer = producer1;
     //  }
     //  catch (error)
     //  {
     //      console.log(error);
     //  }
      
     //  const vtransport = await mediasoupRouters[i].createPlainTransport(aplainTransportOptions);

     //  //console.log(vtransport);
     //  console.log(vtransport.id);
     //  console.log(vtransport.tuple.localIp);
     //  console.log(vtransport.tuple.localPort);

     //  const vrtpParameters =  { 
     //  codecs: 
     //  [
     //      { 
     //          mimeType : "video/h264",  
     //          clockRate :90000,                     
     //          payloadType :100                     
     //      }
     // ],
     //  encodings : [{ ssrc : 2222 }] 
     //  };

     //  try
     //  {
     //      const producer1 = await vtransport.produce(
     //      {
     //          kind: 'video', 
     //          rtpParameters: vrtpParameters
     //      });
     //      vProducers[masterSocket.id] = producer1;
     //      vPlainProducer = producer1;

     //     // console.log(producer1)
     //  }
     //  catch (error)
     //  {
     //      console.log(error);
     //  }


     // startAudioProducerGstreamer(atransport.tuple.localPort);
     //  startVideoProducerGstreamer(vtransport.tuple.localPort);
      

      //Connect Transport 

      //Start Produce

    });





    // masterSocket.on('test2', async(data, callback) => {
    //   console.log("slave test2\n");
    // });


  //masterSocket.request('demo_hello');


}




const cmdEnv = {
    GST_DEBUG:  3,
    ...process.env, // This allows overriding $GST_DEBUG from the shell
  };

function startVideoProducerGstreamer(port) {
  // Return a Promise that can be awaited
  let recResolve;
  const promise = new Promise((res, _rej) => {
    recResolve = res;
  });


  var cmdProgram = "gst-launch-1.0";
  var cmdArgStr = "videotestsrc ! videoconvert ! video/x-raw, format=I420 ! h264enc target-bitrate=1000000 deadline=1 cpu-used=4  ! rtph264pay pt=100 ssrc=2222 picture-id-mode=2 !   udpsink host=127.0.0.1 port="+`${port}`;

 console.log("video cmd "+cmdArgStr);
  let recProcess = Process.spawn(cmdProgram, cmdArgStr.split(/\s+/), {
    env: cmdEnv,
  });

  recProcess.on("error", (err) => {
    console.log("Recording process error:", err);
  });

  recProcess.on("exit", (code, signal) => {
    console.log("Recording process exit, code: %d, signal: %s", code, signal);
  });


  // GStreamer writes some initial logs to stdout
  recProcess.stdout.on("data", (chunk) => {
    chunk
      .toString()
      .split(/\r?\n/g)
      .filter(Boolean) // Filter out empty strings
      .forEach((line) => {
        console.log(line);
        if (line.startsWith("Setting pipeline to PLAYING")) {
          setTimeout(() => {
            recResolve();
          }, 1000);
        }
      });
  });

  // GStreamer writes its progress logs to stderr
  recProcess.stderr.on("data", (chunk) => {
    chunk
      .toString()
      .split(/\r?\n/g)
      .filter(Boolean) // Filter out empty strings
      .forEach((line) => {
        console.log(line);
      });
  });

  return promise;
}
function startAudioProducerGstreamer(port) {
  // Return a Promise that can be awaited
  let recResolve;
  const promise = new Promise((res, _rej) => {
    recResolve = res;
  });


  var cmdProgram = "gst-launch-1.0";
  var cmdArgStr = "audiotestsrc ! audioresample  ! audioconvert ! audio/x-raw, channels=2  ! opusenc ! rtpopuspay pt=101 ssrc=1111 !  udpsink host=127.0.0.1 port="+`${port}`;

 console.log("audio cmd "+cmdArgStr);

  let recProcess = Process.spawn(cmdProgram, cmdArgStr.split(/\s+/), {
    env: cmdEnv,
  });

  recProcess.on("error", (err) => {
    console.log("Recording process error:", err);
  });

  recProcess.on("exit", (code, signal) => {
    console.log("Recording process exit, code: %d, signal: %s", code, signal);
  });


  // GStreamer writes some initial logs to stdout
  recProcess.stdout.on("data", (chunk) => {
    chunk
      .toString()
      .split(/\r?\n/g)
      .filter(Boolean) // Filter out empty strings
      .forEach((line) => {
        console.log(line);
        if (line.startsWith("Setting pipeline to PLAYING")) {
          setTimeout(() => {
            recResolve();
          }, 1000);
        }
      });
  });

  // GStreamer writes its progress logs to stderr
  recProcess.stderr.on("data", (chunk) => {
    chunk
      .toString()
      .split(/\r?\n/g)
      .filter(Boolean) // Filter out empty strings
      .forEach((line) => {
        console.log(line);
      });
  });

  return promise;
}

async function runMediasoupWorker(i) {
  workers[i] = await mediasoup.createWorker({
    logLevel: config.mediasoup.worker.logLevel,
    logTags: config.mediasoup.worker.logTags,
    rtcMinPort: config.mediasoup.worker.rtcMinPort,
    rtcMaxPort: config.mediasoup.worker.rtcMaxPort,
  });

  workers[i].on('died', () => {
    console.log('%d mediasoup worker died, exiting in 2 seconds... [pid:%d]',i,  worker.pid);
    setTimeout(() => process.exit(1), 2000);
  });

  const mediaCodecs = config.mediasoup.router.mediaCodecs;
  mediasoupRouters[i] = await workers[i].createRouter({ mediaCodecs });
}

async function createWebRtcTransport(id) {
  const {
    maxIncomingBitrate,
    initialAvailableOutgoingBitrate
  } = config.mediasoup.webRtcTransport;

  const transport = await mediasoupRouters[id].createWebRtcTransport({
    listenIps: config.mediasoup.webRtcTransport.listenIps,
    enableUdp: true,
    enableTcp: true,
    preferUdp: true,
    initialAvailableOutgoingBitrate,
  });
  if (maxIncomingBitrate) {
    try {
      await transport.setMaxIncomingBitrate(maxIncomingBitrate);
    } catch (error) {
    }
  }
  return {
    transport,
    params: {
      id: transport.id,
      iceParameters: transport.iceParameters,
      iceCandidates: transport.iceCandidates,
      dtlsParameters: transport.dtlsParameters
    },
  };
}

async function createConsumer(id, cid, producer, rtpCapabilities, kind) {

  console.log("consumer called  ", id, kind);

  let avProducer;

  if(kind == 'audio')
    avProducer = audioPipeProducer;
  else if(kind == 'video')
    avProducer = videoPipeProducer;

  
  if (!mediasoupRouters[id].canConsume(
    {
      producerId: avProducer.id,
      rtpCapabilities,
    })
  ) {
    console.log('can not consume');
    return;
  }
  try {
    
    consumer = await cTransports[cid].consume({
      producerId: avProducer.id,
 //     paused: producer.kind === 'video',
      rtpCapabilities
    });


    //console.log("RTP Caps " + rtpCapabilities);


 //    	if(kind == 'audio'){
	//     audioPlainConsumer = await audioPlainTransport.consume({
	//       producerId: producer.id,
	//      rtpCapabilities
	//     });	

	//     console.log("RT " + audioPlainConsumer.rtpParameters.codecs[0].payloadType);
	//     console.log("RT " + audioPlainConsumer.rtpParameters.encodings[0].ssrc);

	//     console.log("plain consume audio call\n");
	// }

	// if(kind == 'video'){
	//     videoPlainConsumer = await videoPlainTransport.consume({
	//       producerId: producer.id,
	//      rtpCapabilities
	//     });	
	//     console.log("plain consume video call\n");
	// }



    //console.log("Plain Consumer ID " + audioPlainConsumer.id);


  } catch (error) {
    console.log('consume failed', error);
    return;
  }

  if (consumer.type === 'simulcast') {
    await consumer.setPreferredLayers({ spatialLayer: 2, temporalLayer: 2 });
  }

return {
    producerId: avProducer.id,
    id: consumer.id,
    kind: consumer.kind,
    rtpParameters: consumer.rtpParameters,
    type: consumer.type,
    producerPaused: consumer.producerPaused
  };

}

