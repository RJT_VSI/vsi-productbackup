var fs = require('fs');
var path = require('path');
const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const https = require('https');
const http = require('http');

// const server = http.Server(app);    
//const server = http.createServer(app);
 
const server = https.createServer({
    key: fs.readFileSync(
      path.resolve(process.env.SSL_DEV_KEY || './ssl/key.pem')
    ),
    cert: fs.readFileSync(
      path.resolve(process.env.SSL_DEV_CRT || './ssl/cert.pem')
    )
  }, app);

const socketIO = require('socket.io');
const io = socketIO(server);
const port = 5001;

io.on('connect', async (socket) => {
    console.log('Client connected');
    socket.emit('hello',"Message from HTTP server.");
});
  
server.listen(port, () => {
    console.log(`started on port: ${port}`);
});
  
io.on('new-message', (message) => {
    io.emit(message);
});

app.use('/', (req, res, next) => {
    res.send('Hello from HTTPS server');
});

// app.use('/', (req, res, next) => {
//     res.send('Hello from HTTP server');
// });
