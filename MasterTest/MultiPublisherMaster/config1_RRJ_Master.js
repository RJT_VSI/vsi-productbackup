module.exports = {
  listenIp: '0.0.0.0',
  listenPort: 5003,
  sslCrt: './certs/server.cert',
  sslKey: './certs/server.key',
  mediasoup: {
    // Worker settings
    worker: {
      rtcMinPort: 12000,
     // rtcMaxPort: 10500,
      rtcMaxPort: 12100,
      logLevel: 'warn',
      logTags: [
        'info',
        'ice',
        'dtls',
        'rtp',
        'srtp',
        'rtcp',
        // 'rtx',
        // 'bwe',
        // 'score',
        // 'simulcast',
        // 'svc'
      ],
    },
    // Router settings
    router: {
      mediaCodecs:
        [
          {
            kind: 'audio',
            mimeType: 'audio/opus',
            clockRate: 48000,
            channels: 2
          },
          {
            kind: 'video',
            mimeType: 'video/VP8',
            clockRate: 90000,
            parameters:
              {
                'x-google-start-bitrate': 1000
              }
          },
        ]
    },
    // WebRtcTransport settings
    webRtcTransport: {
      listenIps: [
        {
          // ip: '192.168.43.182',
          // announcedIp:  '192.168.43.182',

          //ip: '172.31.42.214',
          ip: '10.4.0.4',
          //announcedIp:  '18.217.153.138',
          announcedIp:  '20.81.32.179',
        }
      ],
      maxIncomingBitrate: 1500000,
      initialAvailableOutgoingBitrate: 1000000,
    }

  },

  plainTransport: {
      listenIp: { ip: "0.0.0.0", announcedIp: null },
  },

  pipeTransport:{
    listenIp: { ip: "127.0.0.1", announcedIp: null },
  },


};
