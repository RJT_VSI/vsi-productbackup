const path = require('path');

const express = require('express');
const bodyParser = require('body-parser');
const jwt = require('jsonwebtoken');
const cors=require("cors");

// const session = require('express-session');

const errorController = require('./controllers/error');
const sequelize = require('./util/dataContext');
const User = require('./models/user');
const Policy = require('./models/policy');
const PolicyData = require('./models/policy-data');
const Client = require('./models/client');
const Licence = require('./models/licence');
const Admin = require('./models/Admin');

const authMiddle = require('./middleware/auth');
const errMiddle = require('./middleware/error');

const app = express();
const http = require('http');
// const server = http.Server(app);    
const server = http.createServer(app); 
const socketIO = require('socket.io');
const io = socketIO(server);
const port = 3000;


app.set('view engine', 'ejs');
app.set('views', 'views');

const userRoutes = require('./routes/user');
const authRoutes = require('./routes/auth');
const policyRoutes = require('./routes/policy');
const clientRoutes = require('./routes/client');
const licenceRoutes = require('./routes/licence');
const adminRoutes = require('./routes/admin');
const errRoutes = require('./routes/error');

app.use(express.json());

app.use(function(req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,PATCH,DELETE');
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");

  next();
});
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.static(path.join(__dirname, 'public')));
app.use (express.static(path.join( __dirname,'./dist/VSI-Security')));
app.use(async (req, res, next) => {
  if (req.headers["x-access-token"]) {
   const accessToken = req.headers["x-access-token"];
   const { id, exp } = authMiddle(req,res,next)
   // Check if token has expired
   if (exp < Date.now().valueOf() / 1000) { 
    return res.status(401).json({ error: "JWT token has expired, please login to obtain a new one" });
   } 
   res.locals.loggedInUser = await User.findByPk(id); next(); 
  } else { 
   next(); 
  } 
 });

app.use('/auth', authRoutes);
app.use('/policy',policyRoutes);
app.use('/client',clientRoutes);
app.use('/licence',licenceRoutes);
app.use('/admin',adminRoutes);
app.use(errMiddle.get404);

//one-to-one
Admin.belongsTo(User);
User.hasOne(Admin);
//One-to-many Policy and PolicyData
PolicyData.belongsTo(Policy, { constraints: true, onDelete: 'RESTRICT' });
Policy.hasMany(PolicyData);

sequelize.sync({alter : true});
//.sync({ force: true })

io.on('connect', (socket) => {
  console.log('Client connected');
});

server.listen(port, () => {
  console.log(`started on port: ${port}`);
});

io.on('new-message', (message) => {
io.emit(message);
});

// Send Notification API
app.post('/send-notification', (req, res) => {
  const notify = {data: req.body};
  io.emit('notification', notify); // Updates Live Notification
  res.send(notify);
});
