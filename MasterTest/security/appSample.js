const express = require('express');
const app = express();
const http = require('https');
// const server = http.Server(app);    
const fs = require('fs');

const tls = {
    cert: fs.readFileSync('./certs/server.cert'),
    key: fs.readFileSync('./certs/server.key'),
};


const server = http.createServer(tls, app); 
const socketIO = require('socket.io');
const io = socketIO(server,
{
		     cors: {
			         origins: ['http://localhost:4200']
			       },
/*		   
	  {
    cors: {
     origin: "*",
     methods: ["GET", "POST"],   
     credentials: true
    }
    ,
    */
    serveClient: false,
    path: '/server',
	//transports: ['websocket'],
      //rejectUnauthorized: false,
    log: false,
  }

);
const port = 5001;


io.on('connection', (socket) => {
  console.log('Client connected');
  socket.emit("hello","Message from server");
});

server.listen(port, () => {
  console.log(`started on port: ${port}`);
});

io.on('new-message', (message) => {
	io.emit(message);
})

;
