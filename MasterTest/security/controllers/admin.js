const Admin = require('../models/admin');
const User = require('../models/user')
const sequelize = require("../util/dataContext");
const bcrypt = require('bcryptjs');
const {Op} = require('sequelize');
const jwt = require("jsonwebtoken");
const auth = require("../middleware/auth");
const {roles} = require("../middleware/roles");

exports.postAddAdmin = async(req,res,next) => {
  const email = req.body.email;
  const password = req.body.password;
  const role = req.body.role;

  var userDoc  = await User.findOne({where : {email : email}});
    if (userDoc != null) {
      console.log("Registration failed");
      return res.status(200).json({
        status:false,
        message:"Registration failed",
        error:email+" already exist"
      });
    }
    var hashedPassword = await bcrypt.hash(password, 12);
      const user = new User({
        email: email,
        password: hashedPassword,
        role : 'admin'     
      });
      await user.save();
      var token = jwt.sign({email : user.email,id : user.id},'secret',{expiresIn: "24h"});
      console.log("Token : "+token);
      user.token = token;
      user.createAdmin();
      return res.status(200).json({status : true,message:"Registration successful",data : user});
};