const sequelize = require("../util/dataContext");
const bcrypt = require('bcryptjs');
const User = require('../models/user');
const {Op} = require('sequelize');
const jwt = require("jsonwebtoken");
const auth = require("../middleware/auth");
const {roles} = require("../middleware/roles");
 
exports.grantAccess = function(action, resource) {
 return async (req, res, next) => {
  try {
   const permission = roles.can(req.user.role)[action](resource);
   if (!permission.granted) {
    return res.status(401).json({
      status: false,
      error: "You don't have enough permission to perform this action"
    });
   }
   next()
  } catch (error) {
   next(error)
  }
 }
}
 
exports.allowIfLoggedin = async (req, res, next) => {
 try {
  const user = res.locals.loggedInUser;
  if (!user)
   return res.status(401).json({
    error: "You need to be logged in to access this route"
   });
   req.user = user;
   next();
  } catch (error) {
   next(error);
  }
}

exports.postSignUp = (req,res,next) => {
  const email2 = req.body.email;
  const password = req.body.password;
  const confirmPassword = req.body.confirmPassword;
  const role = req.body.role;

  User.findOne({where : {email : email2}})
  .then(userDoc => {

    if (userDoc != null) {
      console.log("Registration failed");
      return res.status(200).json({
        status:false,
        message:"Registration failed",
        error:email2+" already exist"
      });
    }
    return bcrypt
    .hash(password, 12)
    .then(hashedPassword => {
      const user = new User({
        email: email2,
        passwordHash: hashedPassword,
        role : 'client'     
      });
      user.save();
      var token = jwt.sign({email : user.email,id : user.id},'secret',{expiresIn: "24h"});
      console.log("Token : "+token);
      user.token = token;
      return user;
    })
    .then(result => {   
      res.status(201).json({status:true,message:"Registration successful",data : result})
     });
  })
  .catch(err => console.log(err))
};

exports.postLogin = (req, res, next) => {
  const email = req.body.email;
  const password = req.body.password;
  
  User.findOne({where : { email: email }})
    .then(user => {
      if (!user) {
        return res.json({
          status:false,
          message:"Login failed",
          error:email+" user not found"
      });
      }
      var token = jwt.sign({email : user.email,id : user.id},'secret',{expiresIn: "24h"});
      user.token = token;
      user.save();

      //auth(req,res,next);

      bcrypt
        .compare(password, user.passwordHash)
        .then(doMatch => {
          if (doMatch) {
            return res.json({
              status:true,
              message:"Login successful",
              data : user.token
             });                    
          }
          res.status(401).json({
            status:false,
            message:"Login failed"
           })
          })
        .catch(err => {
          console.log(err);
        });
    })
    .catch(err => console.log(err));
};
