const Client = require('../models/client');
const User = require('../models/user');

exports.postAddClient = async (req, res, next) => {
    const clientPcName = req.body.clientPcName;
    const MAC_id = req.body.MAC_id;
    const licence_key = req.body.licenceKey;

    var client = await Client.create({
        clientPcName : clientPcName,
        Licence_Key : licence_key,
        MAC_Id : MAC_id
    });
    if(client != null)
    {
        return res.status(201).json({status:true,message:"Client added successfully",data:client,code:201});
    }
    return res.status(404).json({
        status:false,
        message:"Client creation failed",
        error:"Client not found",
        code:404
    });
   
}

exports.deleteClient = async (req, res, next) => {
    const clientId = req.params.clientId;
    var result = await Client.destroy({where: {id : clientId}});
        if(result == 1)
        {
            return res.status(200).json({
                status:true,
                message:"Client deleted successfully",
                code:200
              });
        }
        return res.status(404).json({
            status:false,
            message:"Client deletion failed",
            error:"Client not found",
            code:404
        });
};

exports.getClient = async (req,res) => {
    const id = req.params.id;
    var client = await Client.findByPk(id)
    if(client != null)
    {
        return res.status(200).json({status:true,message:"Client found",data:client,code:200});
    }
    return res.status(404).json({
        status:false,
        error:"Client not found",
        code:404
    });
};

exports.getClientByName = async (req,res,next) => {
    const name = req.params.name;
    var client  = await Client.findOne({where:{clientPcName : name}});
    if(client != null)
    {
        return res.status(200).json({status:true,message:"client found",data:client,code:200});
    }
    return res.status(404).json({
        status:false,
        error:"Client not found",
        code:404
    });
}

exports.getAllClient = async (req,res,next) => {
    var clients = await Client.findAll();   
    if(clients.length > 0)
    {
        return res.status(200).json({status:true,message:"Client found",data:clients,code:200});
    }
    return res.status(404).json({
        status:false,
        error:"Client not found",
        code:404
    });
};

exports.editClient = async (req,res,next) => {
    const id = req.body.id;
    const clientPcName = req.body.clientPcName;
    const MAC_Id = req.body.MAC_Id;
    const Licence_Key = req.body.LicenceKey;

    var client  = await Client.findByPk(id)
    if(client != null)
    {
        client.clientPcName = clientPcName;
        client.Licence_Key = Licence_Key;
        client.MAC_Id = MAC_Id;
        await client.save();
        return res.status(200).json({status:true,message:"client edited successfully",data:result,code:200});
    }
    else
    {
        return res.status(404).json({
            status:false,
            message:"Client updation failed",
            error:"Clinet not found",
            code:404
        });
    }
};

exports.addClient = async (req,res) => {
    const clientPcName = req.body.clientPcName;
    const MAC_id = req.body.MAC_id;
    const licence_key = req.body.licenceKey;
    const username = req.body.username

    var userDoc = await User.findOne({where : {username : username}});
    if (userDoc != null) {
        console.log("Registration failed");
        return res.status(200).json({
          status:false,
          message:"Registration failed",
          code:200
        });
    }
    var user = await User.create({
        role : 'client'
    });
    await user.createClient({ 
        clientPcName : clientPcName,
        Licence_Key : licence_key,MAC_Id : MAC_id
    });
    if(user != null)
    {
        return res.status(201).json({status:true,message:"Client created",data:user,code:201});
    }
    return res.status(200).json({status:false,message:"Client not created",code:200});
};
