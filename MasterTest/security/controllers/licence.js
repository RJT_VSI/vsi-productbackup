const Licence = require('../models/licence');

exports.addLicence = async (req,res) => {
    const licenceKey = req.body.licenceKey;
    var licence = await Licence.create({
        Licence_Key: licenceKey
    });
    if(licence != null)
    {
        return res.status(201).json({status:true,message:"Licence created successfully",data:licence,code:201})
    }
    return res.status(200).json({status:false,message:"Licence creation failed",code:200})
}

exports.getLicence = async (req,res,next) => {
    const licenceKey = req.params.licenceKey;
    var licence = await Licence.findOne({where : {Licence_Key : licenceKey }});
    if(licence != null)
    {
        return res.status(200).json({status:true,message:"Licence found",data:licence,code:200});
    }
    return res.status(404).json({
        status:false,
        error:"Licence not found",
        code:404
    });
};

exports.deleteLicence = async (req,res,next) => {
    const licenceKey = req.params.licenceKey;
    var result = await Licence.destroy({where : {Licence_Key : licenceKey}});
        if(licence == 1)
        {
            return res.status(200).json({status:true,message:"Licence Deleted",code:200});
        }
        return res.status(404).json({
            status:false,
            message:"Deletion failed",
            error:"Licence not found",
            code:404
        });
};

exports.getAllLicence = async (req,res,next) => {
    var licences = await Licence.findAll()
        if(licences.length > 0)
        {
            return res.status(200).json({status:true,message:"Licences fetched successfully",data:licences,code:200});
        }
        return res.status(404).json({
            status:false,
            error:"Licence not found",
            code:404
        });
};

exports.editLicence = async (req,res) => {
    const id = req.body.id;
    const licenceKey = req.body.licenceKey;

    var licence = await Licence.findByPk(id)
        if(licence != null){
            licence.Licence_Key = licenceKey;
            await licence.save();
            return res.status(200).json({status:true,message:"Licence found",data:result});
        }
        else{
            return res.status(404).json({
                status:false,
                error:"Licence not found",
                code:404
            });
        }     
};