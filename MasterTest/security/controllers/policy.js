const Policy = require('../models/policy');
const Policy_Data = require('../models/policy-data');
const {Op} = require('sequelize');

exports.postAddPolicy = async (req, res, next) => {
    const policy_name = req.body.policy_name;
    var policy = await Policy.findOne({where:{policy_name:policy_name}})
    if(result){
        return res.status(200).json({status:false,message:"Already exist",code:200});
    }
    const policyCreated = await Policy.create({policy_name : policy_name});
    return res.status(201).json({message:"Policy added successfully",data : policyCreated,code:201});
};

exports.postAddPolicyData = async (req,res,next) => {
    const data = req.body.data;
    const pid = req.body.policyRefId
    var policyData = await Policy_Data.findOne({
        where : {
            [Op.and]: [{policyData : data},{policyId : pid}]
        }
    });
    if(policyData != null)
    {
        return res.status(200).json({status:false,message :"Same data exist already",code:200});
    }
    const policy_data = await Policy_Data.create({ policyData : data, policyId : pid});
    return res.status(201).json({message:"Policy data added successfully",data : policy_data,code:201});
};

exports.getPolicy = async (req, res, next) => {
    const pid = req.params.id;
    var policy = await Policy.findOne({
        where : {
            id : pid
        },
    });
    if(policy != null)
    {
        return res.status(200).json({
            status : true,
            message:"Data found successfully",
            data:policy,
            code:200
        });
    }
    return res.status(404).json({
        status:false,
        error:"Policy not found",
        code:404
    });
};

  exports.deletePolicy = async (req, res) => {
    const id = req.params.id;
  
    var result = await Policy.destroy({where: { id: id }})
        if (result == 1) {
            return res.send({
                status:true,
                message: "Policy was deleted successfully!",
                code:200
            });
        } 
        else {
            return res.status(404).json({
                status:false,
                error:"Policy not found",
                code:404
            });
        }
  };

  exports.getAllPolicies = async (req,res,next) => {
      var policies = await Policy.findAll();
      if(policies.length === 0)
          {
            return res.status(404).json({
                status:false,
                error:"Policies not found",
                code:404
            });
          }
          return res.json({
              status : true,
              message : "Policies found successfully", 
              Data : policies.map(policy => policy)
            });
        };

  exports.softDeletePolicy = async (req,res,next) => {
      const id = req.params.id;
      var policy = await Policy.findByPk(id);
      if(policy != null){
        policy.isDelete = true
        await policy.save();
        return res.status(200).json({status:true,message:"Soft Deletion successful"});
      }
      return res.status(404).json({
        status:false,
        message:"deletion failed",
        error:"Policy not found",
        code:404
    });
  };

exports.getPolicyData = async (req,res,next) => {
    const pid = req.params.id
    var policiesInfo = await Policy_Data.findAll({where : {policyId : pid}});
    if(policiesInfo.length ===0){
        return res.status(404).json({
            status:false,
            error:"Policy-Data not found",
            code:404
        });
    }
    else{
        return res.json({
            status : true,
            message : "Data found successfully", 
            Data : data.map(data => data)
        });
    }
};

exports.getPolicyByName = async (req,res,next) => {
    const name = req.params.name;
    var policy = await Policy.findOne({where:{policy_name : name}});
    if(policy != null)
    {
        return res.json({status:true,message:"Policy found",data:policy});
    }
    return res.status(404).json({
        status:false,
        error:"Policy not found",
        code:404
    });
};


exports.postAddPolicyAsync = async (req, res, next) => {
    const policy_name = req.body.policy_name;
    var result = await Policy.findOne({where:{policy_name:policy_name}});
    if(result != null){
        return res.status(200).json({status:false,message:"Already exist",code:200});
    }
    const policy = await Policy.create({policy_name : policy_name});
    return res.json({message:"Policy added successfully",data : policy});
};