exports.get404 = (req, res, next) => {
  res.status(404).json({
    status:false,
    message:"Page not found",
    code:404
  });
};

exports.get401 = (req, res, next) => {
  res.status(401).json({
    status:false,
    message:"Unauthorized",
    code:404
  });
};

exports.get500 = (req, res, next) => {
  res.status(500).json({
    status:false,
    message:"Internal server error",
    code:500
  });
};

exports.get200 = (req,res,next) => {
  res.status(200).json({
    status:true,
    message:"Ok",
    code:200
  });
};

exports.get201 = (req,res,next) => {
  res.status(201).json({
    status:true,
    message:"Data Created",
    code:200
  });
};
