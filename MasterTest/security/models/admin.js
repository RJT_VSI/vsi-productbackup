const Sequelize = require('sequelize');

const sequelize = require('../util/dataContext');

const Admin = sequelize.define('admin', {
    id: {
      type: Sequelize.UUID,
      defaultValue: Sequelize.UUIDV4,
      allowNull: false,
      primaryKey: true
    },
    MachineName:Sequelize.STRING,
    MAC_Id:Sequelize.STRING,
    updatedAt:{
      type:Sequelize.DATE,
      defaultValue:Sequelize.NOW
    },
    createdAt:{
      type:Sequelize.DATE,
      defaultValue:Sequelize.NOW
    },
    isActive:{
      type:Sequelize.BOOLEAN,
      defaultValue:true
    },
    isDelete:{
      type:Sequelize.BOOLEAN,
      defaultValue:true
    },
    userId:{
        type: Sequelize.UUID,
        references: {
            model: 'users',
            key: 'id'
        }
    },
    createdBy:Sequelize.STRING
  });
  
  module.exports = Admin;