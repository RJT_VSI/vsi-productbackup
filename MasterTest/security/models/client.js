const Sequelize = require('sequelize');

const sequelize = require('../util/dataContext');

const Client = sequelize.define('client', {
  id: {
    type: Sequelize.UUID,
    defaultValue: Sequelize.UUIDV4,
    allowNull: false,
    primaryKey: true
  },
  clientPcName: Sequelize.STRING,
  MAC_Id:Sequelize.STRING,
  
  updatedAt:{
    type:Sequelize.DATE,
    defaultValue:Sequelize.NOW
  },
  createdAt:{
    type:Sequelize.DATE,
    defaultValue:Sequelize.NOW
  },
  isActive:{
    type:Sequelize.BOOLEAN,
    defaultValue:true
  },
  isDelete:{
    type:Sequelize.BOOLEAN,
    defaultValue:true
  },
  createdBy:Sequelize.STRING
});

module.exports = Client;