const Sequelize = require('sequelize');

const sequelize = require('../util/dataContext');

const Licence = sequelize.define('licence', {
  id: {
    type: Sequelize.UUID,
    defaultValue: Sequelize.UUIDV4,
    allowNull: false,
    primaryKey: true
  },
  Licence_Key:Sequelize.STRING,
  updatedAt:Sequelize.DATE,
  createdAt:Sequelize.DATE,
  createdBy:Sequelize.STRING,
  isActive:{
    type:Sequelize.BOOLEAN,
    defaultValue:true
  },
  isDelete:{
    type:Sequelize.BOOLEAN,
    defaultValue:true
  },
});

module.exports = Licence;