const Sequelize = require('sequelize');

const sequelize = require('../util/dataContext');

const Policy = sequelize.define('policy', {
  id:{
      type: Sequelize.UUID,
      defaultValue: Sequelize.UUIDV4,
      allowNull: false,
      primaryKey: true
    },
  policy_name: Sequelize.STRING,
  updatedAt:{
    type:Sequelize.DATE,
    defaultValue:Sequelize.NOW
  },
  createdAt:{
    type:Sequelize.DATE,
    defaultValue:Sequelize.NOW
  },
  isActive:{
    type:Sequelize.BOOLEAN,
    defaultValue:true
  },
  isDelete:{
    type:Sequelize.BOOLEAN,
    defaultValue:true
  },
});

module.exports = Policy;