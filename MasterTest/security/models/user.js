const Sequelize = require('sequelize');

const sequelize = require('../util/dataContext');

const User = sequelize.define('user', {
    id: {
      type: Sequelize.UUID,
      defaultValue: Sequelize.UUIDV4,
      allowNull: false,
      primaryKey: true
    },
    name: Sequelize.STRING,
    email: Sequelize.STRING,
    username:{
      type:Sequelize.STRING,
      unique : true
    },
    role:{
      type:Sequelize.ENUM('admin','client'),
      defaultValue : 'admin'
    },
    passwordHash:Sequelize.STRING,
    token:Sequelize.STRING,
    createdBy:Sequelize.STRING,
    updatedAt:{
      type:Sequelize.DATE,
      defaultValue:Sequelize.NOW
    },
    createdAt:{
      type:Sequelize.DATE,
      defaultValue:Sequelize.NOW
    },
    isActive:{
      type:Sequelize.BOOLEAN,
      defaultValue:true
    },
    isDelete:{
      type:Sequelize.BOOLEAN,
      defaultValue:true
    },
  });
  
  module.exports = User;