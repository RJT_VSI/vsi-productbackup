const path = require('path');

const express = require('express');

const adminController = require('../controllers/admin');

const router = express.Router();

router.post('/add-admin', adminController.postAddAdmin);

module.exports = router;
