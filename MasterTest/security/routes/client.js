const path = require('path');

const express = require('express');

const clientController = require('../controllers/client');
const { client } = require('websocket');

const router = express.Router();

router.post('/add-client', clientController.postAddClient);

router.post('/add-client-user',clientController.addClient);

router.delete('/delete-client/:clientId',clientController.deleteClient);

router.get('/get-client/:id',clientController.getClient);

router.get('/get-client-by-name/:name',clientController.getClientByName);

router.get('/get-all-client',clientController.getAllClient);

router.post('/edit-client',clientController.editClient);

module.exports = router;