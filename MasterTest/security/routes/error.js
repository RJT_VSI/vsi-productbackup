const express = require('express');

const errMiddle = require('../middleware/error');

const auth = require('../middleware/auth');

const router = express.Router();

router.get('/500',errMiddle.get500);

router.get('/200',errMiddle.get200);

router.get('/201',errMiddle.get201);

router.get('/401',errMiddle.get401);

router.get('/404',errMiddle.get404);

module.exports = router;