const path = require('path');

const express = require('express');

const licenceController = require('../controllers/licence');

const router = express.Router();

router.post('/add-licence',licenceController.addLicence);

router.get('/get-licence/:licenceKey',licenceController.getLicence)

router.delete('/delete-licence/:licenceKey',licenceController.deleteLicence);

router.get('/get-all-licence',licenceController.getAllLicence);

router.post('/edit-licence',licenceController.editLicence);

module.exports = router;