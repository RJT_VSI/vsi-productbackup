const express = require('express');
const policyController = require('../controllers/policy');
const authController = require('../controllers/auth');
const router = express.Router();

router.post('/add-policy', policyController.postAddPolicy);

router.post('/add-policy-async', policyController.postAddPolicyAsync);

router.get('/get-policy/:id',policyController.getPolicy);  //authController.allowIfLoggedin,authController.grantAccess('readAny','profile')

router.get('/get-policy-data/:id',policyController.getPolicyData);

router.delete('/delete-policy/:id',policyController.deletePolicy);

router.get('/get-all-policy',policyController.getAllPolicies);

router.delete('/soft-delete-policy/:id',policyController.softDeletePolicy);

router.post('/add-policy-data', policyController.postAddPolicyData);

router.get('/get-policy-by-name/:name',policyController.getPolicyByName);

module.exports = router;