process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";
const mediasoup = require('mediasoup');
const fs = require('fs');
const https = require('https');
const express = require('express');
const socketIO = require('socket.io');
//const config = require('./config1');
const config = require('./config1_RRJ_Master');


// Global variables
let worker;
let webServer;
let socketServer;
let expressApp;
let producer;
let consumer;
let producerTransport;
let consumerTransport;
let mediasoupRouter;
let vProducer;
let aProducer;

var mediasoupRouters =  new Array();
var workers = new Array();
var worker_status = new Array();
var publish_dict = {};
var vProducers = {};
var aProducers = {};
var pTransports = {};
var cTransports = {};
var mapId = {};


(async () => {
  try {
    await runExpressApp();
    await runWebServer();
    await runSocketServer();
    for (var i=0; i<10; i++)
    {
      worker_status[i] = 0; 
      await runMediasoupWorker(i);
    }
//    await runMediasoupWorker();
  } catch (err) {
    console.error(err);
  }
})();

async function runExpressApp() {
  expressApp = express();
  expressApp.use(express.json());
  expressApp.use(express.static(__dirname));

  expressApp.use((error, req, res, next) => {
    if (error) {
      console.warn('Express app error,', error.message);

      error.status = error.status || (error.name === 'TypeError' ? 400 : 500);

      res.statusMessage = error.message;
      res.status(error.status).send(String(error));
    } else {
      next();
    }
  });
}

async function runWebServer() {
  const { sslKey, sslCrt } = config;
  if (!fs.existsSync(sslKey) || !fs.existsSync(sslCrt)) {
    console.error('SSL files are not found. check your config.js file');
    process.exit(0);
  }
  const tls = {
    cert: fs.readFileSync(sslCrt),
    key: fs.readFileSync(sslKey),
  };
  webServer = https.createServer(tls, expressApp);
 // webServer = https.createServer( expressApp);
  webServer.on('error', (err) => {
    console.error('starting web server failed:', err.message);
  });

  await new Promise((resolve) => {
    
    const { listenIp, listenPort } = config;

    webServer.listen(listenPort, listenIp, () => {
      const listenIps = config.mediasoup.webRtcTransport.listenIps[0];
      const ip = listenIps.announcedIp || listenIps.ip;
      console.log('server is running');
      console.log(`open https://${ip}:${listenPort} in your web browser`);
      resolve();
    });
    
  });
}

async function runSocketServer() {
  socketServer = socketIO(webServer, 
	   {
		     cors: {
			         origins: ['http://localhost:4200']
			       },
/*		   
	  {
    cors: {
     origin: "*",
     methods: ["GET", "POST"],   
     credentials: true
    }
    ,
    */
    serveClient: false,
    path: '/server',
    log: false,
  });

  socketServer.on('connection', (socket) => {
    console.log('client connected --');  
    socket.emit("ack");
    socket.on('subscribe', async(data, callback) => {
      console.log('subscribed       ', data.id);
      count = 1;
      for (i in cTransports)
        count++;
      socket.broadcast.emit ("user", {name:data.name, total:count});
      var did;
      if (data.id in mapId)
         did = mapId[data.id] 
         if (did in publish_dict)
             socket.emit ("newProducer");
      callback (data);
    });
      
    socket.on('disconnect', () => {
      console.log ('disconnect called');
      if (socket.id in publish_dict)
      {
         const i =  publish_dict[socket.id];
         worker_status[i] = 0;
         delete publish_dict[socket.id];
          console.log ('stopStream Called');
         socket.broadcast.emit ("stopStream");
      }
      else if (socket.id in cTransports)
      {
        console.log ('closing the client transport ');      
        cTransports[socket.id].close();
        delete cTransports[socket.id];
        count = 0;
        for (i in cTransports)
            count++;
        socket.broadcast.emit ("user_remove", {name:socket.id, total:count});
      }
       console.log('client disconnected');
    });

    socket.on('connect_error', (err) => {
      console.error('client connection error', err);
    });

    socket.on('close_client', () => {
      console.log ('@@@@@@ socket close');
	    socket.destroy();
    });
    socket.on('add_class', async(data, callback) => {
      console.log('Adding class  ', socket.id, data.id);
      console.log (socket.id);
      mapId[data.id] = socket.id;
              
      for (var i=0; i<10; i++)
      {
          if (worker_status[i] == 0)
          {
              publish_dict[socket.id] = i;
              worker_status[i] = 1;
              break;
          }
      }                
      callback ({id:socket.id});
    });

    socket.on('register_client', async(data, callback) => {  	  
      console.log('Adding client  ', socket.id);
      console.log (socket.id);
      callback ({id:socket.id});
    });
      
    socket.on('resume_streams', async(data, callback) => {
      socket.broadcast.emit('refresh_streams');
      callback ({id:data.id});
    });

    socket.on('resume_audio', async(data, callback) => {
      socket.broadcast.emit('refresh_audio');
      callback ({id:data.id});
    });

    socket.on('resume_video', async(data, callback) => {
      socket.broadcast.emit('refresh_video');
      callback ({id:data.id});
    });

    socket.on('notify_stopvideo', async(data, callback) => {
      socket.broadcast.emit('removeVideo');
      callback ({id:data.id});
    });

    socket.on('getRouterRtpCapabilities', (data, callback) => {
      var did;
      if (data.id in mapId)
      {
          did = mapId[data.id];
      }
      else
      {
          callback({ error: "Meeting not started yet please try after some time "+data.id });  
          return;
      }
        
      if (did in publish_dict)
      {
          const id = publish_dict[did];
          console.log ("called   ", data.id);
          callback(mediasoupRouters[id].rtpCapabilities);
      }
      else
      {      
        callback({ error: "Meeting not started yet please try after some time" });
      }
    });

    socket.on('createProducerTransport', async (data, callback) => {
      try {
          console.log ("create Producer Transport");
          const did = mapId[data.id];
          if (did in publish_dict)
          {
              const id = publish_dict[did];
              const { transport, params } = await createWebRtcTransport(id);
              producerTransport = transport;
              pTransports[did] = transport;
              callback(params);
          }
          else
          {
              console.error ("No producer available");
          }
      } catch (err) {
        console.error(err);
        callback({ error: err.message });
      }
    });

    socket.on('createConsumerTransport', async (data, callback) => {
      try {
          const did = mapId[data.id];
          console.log ("create Consumer Transport ", did, data.id);
          if (did in publish_dict)
          {
            const id = publish_dict[did];
            const { transport, params } = await createWebRtcTransport(id);
            consumerTransport = transport;
            cTransports[data.client_id] = transport;
            callback(params);
          }
          else
          {
            console.error ("No published id present");
            callback ({error: "No publisher id"})

          }
       } catch (err) {
        console.error(err);
        callback({ error: err.message });
       }
    });

    socket.on('connectProducerTransport', async (data, callback) => {
          const did = mapId[data.id];
          pTransport = pTransports[did]; 
          console.log ("connecting producer transport");
          await producerTransport.connect({ dtlsParameters: data.dtlsParameters });
          callback();
          console.log ("connecting producer transport done");
    });

    socket.on('connectConsumerTransport', async (data, callback) => {
      if (data.client_id in cTransports)
      {
          console.log ("connecting consumer tranaport");
          await cTransports[data.client_id].connect({ dtlsParameters: data.dtlsParameters });
          callback();
          console.log ("connecting consumer transport");
      }
    });

    socket.on('produce', async (data, callback) => {
      const {id, kind, rtpParameters} = data;
      const did = mapId[id];
      producer = await producerTransport.produce({ kind, rtpParameters });
      if (kind == 'video')
      {
        vProducer = producer;
        vProducers[did] = vProducer;
      }
      else
      {
        aProducer = producer;
        aProducers[did] = aProducer;
      }
      callback({ id: producer.id });
      console.log ("Producer called with ", producer.kind);
    });

    socket.on('consume', async (data, callback) => {
      callback(await createConsumer(0, 0, producer, data.rtpCapabilities));
    });

    socket.on('consume_video', async (data, callback) => {
      const did = mapId[data.id];
      console.log ("Publisher ID   ", data.id, did, data.client_id);
      if (did in publish_dict)
      {	      
	  if (did in vProducers)
	  {
             vProducer = vProducers[did];
	     const id = publish_dict[did];
	     callback(await createConsumer(id, data.client_id,  vProducer, data.rtpCapabilities));
	  }
	  else
	  {	  
             callback({error: "no video producer"});
	     return;	  
	  }
      }
      else
      {
         callback({error: "no video producer"});
	 return;	  
      }
    });

    socket.on('consume_audio', async (data, callback) => {
      console.log ("A Publisher ID   ", data.id);
      const did = mapId[data.id];
      if (did in publish_dict)
      {
	  if (did in aProducers)
	  {
             aProducer = aProducers[did];
             const id = publish_dict[did];
             callback(await createConsumer(id, data.client_id, aProducer, data.rtpCapabilities));
	  }
	  else
	  {
             callback({error: "no audio producer"});
	     return;	  
	  }
      }
      else
      {
         callback({error: "no audio producer"});
	 return;	  
      }
    });
    socket.on('resume', async (data, callback) => {
      await consumer.resume();
      callback();
    });
  });
}

async function runMediasoupWorker(i) {
  workers[i] = await mediasoup.createWorker({
    logLevel: config.mediasoup.worker.logLevel,
    logTags: config.mediasoup.worker.logTags,
 	rtcMinPort: config.mediasoup.worker.rtcMinPort,
   rtcMaxPort: config.mediasoup.worker.rtcMaxPort,
  });

  workers[i].on('died', () => {
    console.error('%d mediasoup worker died, exiting in 2 seconds... [pid:%d]',i,  worker.pid);
    setTimeout(() => process.exit(1), 2000);
  });

  const mediaCodecs = config.mediasoup.router.mediaCodecs;
  mediasoupRouters[i] = await workers[i].createRouter({ mediaCodecs });
}

async function createWebRtcTransport(id) {
  const {
    maxIncomingBitrate,
    initialAvailableOutgoingBitrate
  } = config.mediasoup.webRtcTransport;

  const transport = await mediasoupRouters[id].createWebRtcTransport({
    listenIps: config.mediasoup.webRtcTransport.listenIps,
    enableUdp: true,
    enableTcp: true,
    preferUdp: true,
    initialAvailableOutgoingBitrate,
  });
  if (maxIncomingBitrate) {
    try {
      await transport.setMaxIncomingBitrate(maxIncomingBitrate);
    } catch (error) {
    }
  }
  return {
    transport,
    params: {
      id: transport.id,
      iceParameters: transport.iceParameters,
      iceCandidates: transport.iceCandidates,
      dtlsParameters: transport.dtlsParameters
    },
  };
}

async function createConsumer(id, cid, producer, rtpCapabilities) {
  //console.log ("consumer called  ", producer);
  if (!mediasoupRouters[id].canConsume(
    {
      producerId: producer.id,
      rtpCapabilities,
    })
  ) {
    console.error('can not consume');
    return;
  }
  try {
    consumer = await cTransports[cid].consume({
      producerId: producer.id,
 //     paused: producer.kind === 'video',
      rtpCapabilities
    });
  } catch (error) {
    console.error('consume failed', error);
    return;
  }

  if (consumer.type === 'simulcast') {
    await consumer.setPreferredLayers({ spatialLayer: 2, temporalLayer: 2 });
  }

  return {
    producerId: producer.id,
    id: consumer.id,
    kind: consumer.kind,
    rtpParameters: consumer.rtpParameters,
    type: consumer.type,
    producerPaused: consumer.producerPaused
  };
}

