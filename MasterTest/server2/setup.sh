#!/bin/sh

# This file must be placed along with config.js and server.js

filename="config.js"
indexfile="index.html"

listenIpSearchString="<listen-ip>"
privateIpSearchString="<private-ip>"
publicIpSearchString="<public-ip>"

privateIpSearchHTML="private-ip"
publicIpSearchHTML="public-ip"
hostnameSearchHTML="hostname"

publicIp=$(curl ifconfig.co)
privateIp=$(hostname -I)
listenIp="0.0.0.0"
hostname=$(hostname)

echo $publicIp
echo $privateIp
echo $listenIp

sed -i "s/${listenIpSearchString}/${listenIp}/" ${filename}
sed -i "s/${privateIpSearchString}/${privateIp}/" ${filename}
sed -i "s/${publicIpSearchString}/${publicIp}/" ${filename}

sed -i "s/${privateIpSearchHTML}/${privateIp}" ${indexfile}
sed -i "s/${publicIpSearchHTML}/${publicIp}" ${indexfile}
sed -i "s/${hostnameSearchHTML}/${hostname}" ${indexfile}

forever start server.js&

