#!/bin/sh

# Get the domain name label from metadata.
instanceName=$(curl -H Metadata:true --noproxy "*" "http://169.254.169.254/metadata/instance/compute?api-version=2021-03-01" | jq -r '.name')
location=$(curl -H Metadata:true --noproxy "*" "http://169.254.169.254/metadata/instance/compute?api-version=2021-03-01" | jq -r '.location')
vmIndex=$(echo $instanceName | cut -d '_' -f 2)
specifiedVmssDomainNameLabel="vsiedumultimediaworkertest"
fqdn="vm${vmIndex}.${specifiedVmssDomainNameLabel}.${location}.cloudapp.azure.com"

certDirPath="/etc/letsencrypt/live/${fqdn}"

if [ -d "$certDirPath" ]; then
    sudo echo "Certificates already installed."
else
    # Install the SSL certificate for the obtained domain name.
    sudo certbot certonly --nginx -d $fqdn -n
fi

port="5005"
defaultDnsString="<dns-name-label>"
defaultPortString="<port>"

# Configure the nginx server and reverse proxy configuration.
nginxTemplate=$(sudo cat /home/vsiedu/vsiedu/streamserver/vsi-product/nginx-template.txt)

dnsChanged=$(sudo echo $nginxTemplate | sed -e "s/${defaultDnsString}/${fqdn}/g")
portChanged=$(sudo echo $dnsChanged | sed -e "s/${defaultPortString}/${port}/g")

sudo echo "$portChanged" > "/etc/nginx/sites-available/default"

sudo nginx -t
sudo service nginx restart

# Copy the following files to their appropriate location.
serverFilePath="/home/vsiedu/vsiedu/streamserver/vsi-product/Slave/server_v4_RRJ_Slave.js"
configFilePath="/home/vsiedu/vsiedu/streamserver/vsi-product/Slave/config1_RRJ_Slave.js"
subscribeHTMLFilePath="/home/vsiedu/vsiedu/streamserver/vsi-product/Slave/subscribe.html"
subscribeappFilePath="/home/vsiedu/vsiedu/streamserver/vsi-product/Slave/subscribeapp.js"

destServerFilePath="/home/vsiedu/vsiedu/streamserver/server/server_v4_RRJ_Slave.js"
destConfigFilePath="/home/vsiedu/vsiedu/streamserver/server/config1_RRJ_Slave.js"
destSubscribeHTMLFilePath="/home/vsiedu/vsiedu/streamserver/server/subscribe.html"
destSubscribeappFilePath="/home/vsiedu/vsiedu/streamserver/server/subscribeapp.js"

yes | cp $serverFilePath $destServerFilePath
yes | cp $configFilePath $destConfigFilePath
yes | cp $subscribeHTMLFilePath $destSubscribeHTMLFilePath
yes | cp $subscribeappFilePath $destSubscribeappFilePath

privateIpSearchString="<private-ip>"
publicIpSearchString="<public-ip>"

privateIpSearchHTML="private-ip"
publicIpSearchHTML="public-ip"

publicIp=$(dig +short myip.opendns.com @resolver1.opendns.com)
echo $publicIp

privateIp=$(hostname -I | awk '{print $1}')
echo $privateIp

# Make the config file and HTML file changes.
sed -i "s/${privateIpSearchString}/${privateIp}/" ${destConfigFilePath}
sed -i "s/${publicIpSearchString}/${publicIp}/" ${destConfigFilePath}
sed -i "s/${publicIpSearchString}/${publicIp}/" ${destSubscribeappFilePath}

sed -i "s/${privateIpSearchHTML}/${privateIp}/" ${destSubscribeHTMLFilePath}
sed -i "s/${publicIpSearchHTML}/${publicIp}/" ${destSubscribeHTMLFilePath}

# Start the node server.
cd /home/vsiedu/vsiedu/streamserver/server/
#node server_v4_RRJ_Slave.js & > output.txt
forever start server_v4_RRJ_Slave.js
