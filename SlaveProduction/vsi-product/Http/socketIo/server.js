const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const http = require('http');
// const server = http.Server(app);    
const server = http.createServer(app); 
const socketIO = require('socket.io');
const io = socketIO(server);
const port = 3000;
const ip = "192.168.1.197"

io.on('connect', async (socket) => {
    console.log('Client connected');
    socket.emit('hello',"Message from HTTP server.");
});
  
server.listen(port, ip, () => {
    console.log(`started on port: ${port}`);
});
  
io.on('new-message', (message) => {
    io.emit(message);
});

app.use('/', (req, res, next) => {
    res.send('Hello from HTTP server');
});