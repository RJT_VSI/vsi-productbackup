module.exports = {
  listenIp: '0.0.0.0',
  listenPort: 5007,
  sslCrt: './certs/server.cert',
  sslKey: './certs/server.key',
  mediasoup: {
    // Worker settings
    worker: {
      rtcMinPort: 14000,
      rtcMaxPort: 15000,
      logLevel: 'warn',
      logTags: [
        'info',
        'ice',
        'dtls',
        'rtp',
        'srtp',
        'rtcp',
        // 'rtx',
        // 'bwe',
        // 'score',
        // 'simulcast',
        // 'svc'
      ],
    },
    // Router settings
    router: {
      mediaCodecs: [
        {
          kind: 'audio',
          mimeType: 'audio/opus',
          clockRate: 48000,
          channels: 2
        },
        {
          kind: 'video',
          mimeType: 'video/VP8',
          clockRate: 90000,
          parameters: {
            'x-google-start-bitrate': 1000
          }
        },
      ]
    },
    // WebRtcTransport settings
    webRtcTransport: {
      listenIps: [
        {
          ip: '10.0.0.6',
          announcedIp:'40.76.209.98',
        }
      ],
      maxIncomingBitrate: 1500000,
      initialAvailableOutgoingBitrate: 1000000,
    }
  },
  plainTransport: {
    listenIp: { 
      ip: "0.0.0.0",
      announcedIp: null
    },
  }
};
